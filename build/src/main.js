"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* eslint-disable @typescript-eslint/no-this-alias */
const cache = require("memory-cache");
const tronity = require("tronity-platform");
const utils = require("@iobroker/adapter-core");
class Tronity extends utils.Adapter {
    constructor(options = {}) {
        super(Object.assign(Object.assign({}, options), { name: 'tronity' }));
        this.on('ready', this.onReady.bind(this));
        this.on('message', this.onMessage.bind(this));
        this.on('stateChange', this.onStateChange.bind(this));
        this.on('unload', this.onUnload.bind(this));
        // Timeouts and intervals
        this.GetAllInfoTimeout = null;
    }
    /**
     * Is called when databases are connected and adapter received configuration.
     */
    onReady() {
        return __awaiter(this, void 0, void 0, function* () {
            this.log.debug('Starting Tronity');
            // eslint-disable-next-line @typescript-eslint/no-this-alias
            const Adapter = this;
            this.subscribeStates('command.*');
            yield Adapter.setStateAsync('info.connection', false, true);
            if (this.config.client_id && this.config.client_secret && this.config.vehicle_id) {
                yield Adapter.setStateAsync('info.connection', true, true);
                yield Adapter.initSetObject('command.Charging', 'boolean', 'data');
                yield Adapter.initSetObject('level', 'number', 'data');
                yield Adapter.initSetObject('usableLevel', 'number', 'data');
                yield Adapter.initSetObject('range', 'number', 'data');
                yield Adapter.initSetObject('speed', 'number', 'data');
                yield Adapter.initSetObject('power', 'number', 'data');
                yield Adapter.initSetObject('chargerPower', 'number', 'data');
                yield Adapter.initSetObject('phases', 'number', 'data');
                yield Adapter.initSetObject('voltage', 'number', 'data');
                yield Adapter.initSetObject('current', 'number', 'data');
                yield Adapter.initSetObject('energyAdded', 'number', 'data');
                yield Adapter.initSetObject('milesAdded', 'number', 'data');
                yield Adapter.initSetObject('socMax', 'number', 'data');
                yield Adapter.initSetObject('consumption', 'number', 'data');
                yield Adapter.initSetObject('outTemp', 'number', 'data');
                yield Adapter.initSetObject('inTemp', 'number', 'data');
                yield Adapter.initSetObject('passTemp', 'number', 'data');
                yield Adapter.initSetObject('limitSoc', 'number', 'data');
                yield Adapter.initSetObject('fan', 'number', 'data');
                yield Adapter.initSetObject('climate', 'boolean', 'data');
                yield Adapter.initSetObject('seatHeating', 'number', 'data');
                yield Adapter.initSetObject('preconditioning', 'boolean', 'data');
                yield Adapter.initSetObject('charging', 'string', 'data');
                yield Adapter.initSetObject('driving', 'boolean', 'data');
                yield Adapter.initSetObject('doorLocked', 'boolean', 'data');
                yield Adapter.initSetObject('sentryMode', 'boolean', 'data');
                yield Adapter.initSetObject('windowsLocked', 'boolean', 'data');
                yield Adapter.initSetObject('odometer', 'number', 'data');
                yield Adapter.initSetObject('latitude', 'string', 'data');
                yield Adapter.initSetObject('longitude', 'string', 'data');
                yield Adapter.setStateAsync('command.Charging', false, true);
                yield Adapter.GetAllInfoTask();
            }
        });
    }
    initSetObject(name, type, role) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.setObjectNotExistsAsync(name, {
                type: 'state',
                common: {
                    name,
                    type,
                    role,
                    write: true,
                    read: true
                },
                native: {}
            });
        });
    }
    getToken() {
        return __awaiter(this, void 0, void 0, function* () {
            const Adapter = this;
            try {
                if (cache.get(Adapter.config.client_id)) {
                    return cache.get(Adapter.config.client_id);
                }
                const token = yield new tronity.AuthenticationApi().authControllerAuthentication({
                    client_id: Adapter.config.client_id,
                    client_secret: Adapter.config.client_secret,
                    grant_type: 'app'
                });
                cache.put(Adapter.config.client_id, token.data.access_token, 1000 * 60 * 58);
                return token.data.access_token;
            }
            catch (e) {
                Adapter.log.error(e);
            }
        });
    }
    GetAllInfoTask() {
        return __awaiter(this, void 0, void 0, function* () {
            const Adapter = this;
            try {
                if (Adapter.config.vehicle_id) {
                    const accessToken = yield this.getToken();
                    const record = yield new tronity.VehiclesApi({
                        accessToken
                    }).getManyBaseRecordControllerRecord(Adapter.config.vehicle_id, undefined, undefined, ['createdAt,DESC'], undefined, 1, undefined);
                    const recordData = record.data.data;
                    if (recordData.length > 0) {
                        Adapter.setState('level', recordData[0].level, true);
                        if (recordData[0].usableLevel)
                            Adapter.setState('usableLevel', recordData[0].usableLevel, true);
                        if (recordData[0].range)
                            Adapter.setState('range', recordData[0].range, true);
                        if (recordData[0].speed)
                            Adapter.setState('speed', recordData[0].speed, true);
                        if (recordData[0].power)
                            Adapter.setState('power', recordData[0].power, true);
                        if (recordData[0].chargerPower)
                            Adapter.setState('chargerPower', recordData[0].chargerPower, true);
                        if (recordData[0].phases)
                            Adapter.setState('phases', recordData[0].phases, true);
                        if (recordData[0].voltage)
                            Adapter.setState('voltage', recordData[0].voltage, true);
                        if (recordData[0].current)
                            Adapter.setState('current', recordData[0].current, true);
                        if (recordData[0].energyAdded)
                            Adapter.setState('energyAdded', recordData[0].energyAdded, true);
                        if (recordData[0].milesAdded)
                            Adapter.setState('milesAdded', recordData[0].milesAdded, true);
                        if (recordData[0].socMax)
                            Adapter.setState('socMax', recordData[0].socMax, true);
                        if (recordData[0].consumption)
                            Adapter.setState('consumption', recordData[0].consumption, true);
                        if (recordData[0].outTemp)
                            Adapter.setState('outTemp', recordData[0].outTemp, true);
                        if (recordData[0].inTemp)
                            Adapter.setState('inTemp', recordData[0].inTemp, true);
                        if (recordData[0].passTemp)
                            Adapter.setState('passTemp', recordData[0].passTemp, true);
                        if (recordData[0].limitSoc)
                            Adapter.setState('limitSoc', recordData[0].limitSoc, true);
                        if (recordData[0].fan)
                            Adapter.setState('fan', recordData[0].fan, true);
                        if (recordData[0].climate)
                            Adapter.setState('climate', recordData[0].climate, true);
                        if (recordData[0].seatHeating)
                            Adapter.setState('seatHeating', recordData[0].seatHeating, true);
                        if (recordData[0].preconditioning)
                            Adapter.setState('preconditioning', recordData[0].preconditioning, true);
                        if (recordData[0].charging)
                            Adapter.setState('charging', recordData[0].charging, true);
                        if (recordData[0].driving)
                            Adapter.setState('driving', recordData[0].driving, true);
                        if (recordData[0].doorLocked)
                            Adapter.setState('doorLocked', recordData[0].doorLocked, true);
                        if (recordData[0].sentryMode)
                            Adapter.setState('sentryMode', recordData[0].sentryMode, true);
                        if (recordData[0].windowsLocked)
                            Adapter.setState('windowsLocked', recordData[0].windowsLocked, true);
                        if (recordData[0].odometer)
                            Adapter.setState('odometer', recordData[0].odometer, true);
                        Adapter.setState('latitude', recordData[0].latitude.toString(), true);
                        Adapter.setState('longitude', recordData[0].longitude.toString(), true);
                    }
                }
            }
            catch (e) {
                Adapter.log.error(e);
            }
            this.GetAllInfoTimeout = setTimeout(() => Adapter.GetAllInfoTask(), 60 * 1000);
        });
    }
    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    onMessage(msg) {
        return __awaiter(this, void 0, void 0, function* () {
            // eslint-disable-next-line @typescript-eslint/no-this-alias
            const Adapter = this;
            if (msg.command === 'validate') {
                const client_id = msg.message.client_id;
                const client_secret = msg.message.client_secret;
                Adapter.log.info('Try to validate login data and get vehicles');
                try {
                    const token = yield new tronity.AuthenticationApi().authControllerAuthentication({
                        client_id,
                        client_secret,
                        grant_type: 'app'
                    });
                    const vehicles = yield new tronity.VehiclesApi({
                        accessToken: token.data.access_token
                    }).getManyBaseVehicleControllerVehicle();
                    Adapter.sendTo(msg.from, msg.command, { success: true, vehicles: vehicles.data.data }, msg.callback);
                }
                catch (e) {
                    Adapter.sendTo(msg.from, msg.command, { success: false }, msg.callback);
                }
            }
        });
    }
    onStateChange(id, state) {
        return __awaiter(this, void 0, void 0, function* () {
            const Adapter = this;
            if (!state)
                return;
            Adapter.log.debug(`State Change: ${id} to ${state.val} ack ${state.ack}`);
            const State = yield Adapter.getStateAsync('info.connection');
            if (!State || !State.val) {
                Adapter.log.warn('You tried to set a State, but there is currently no valid Token, please configure Adapter first!');
                return;
            }
            const currentId = id.substring(Adapter.namespace.length + 1);
            switch (currentId) {
                case 'command.Charging':
                    if (state.val) {
                        try {
                            const accessToken = yield this.getToken();
                            yield new tronity.VehiclesApi({
                                accessToken
                            }).vehicleControllerChargeStart(Adapter.config.vehicle_id);
                            Adapter.log.info('Try to start charging!');
                        }
                        catch (e) {
                            Adapter.log.error(e);
                        }
                    }
                    else {
                        try {
                            const accessToken = yield this.getToken();
                            yield new tronity.VehiclesApi({
                                accessToken
                            }).vehicleControllerChargeStop(Adapter.config.vehicle_id);
                            Adapter.log.info('Try to stop charging!');
                        }
                        catch (e) {
                            Adapter.log.error(e);
                        }
                    }
                    break;
            }
        });
    }
    /**
     * Is called when adapter shuts down - callback has to be called under any circumstances!
     */
    onUnload(callback) {
        const Adapter = this;
        try {
            this.log.info('cleaned everything up...');
            if (Adapter.GetAllInfoTimeout) {
                clearTimeout(Adapter.GetAllInfoTimeout);
            }
            callback();
        }
        catch (e) {
            callback();
        }
    }
}
if (module.parent) {
    // Export the constructor in compact mode
    module.exports = (options) => new Tronity(options);
}
else {
    // otherwise start the instance directly
    (() => new Tronity())();
}
